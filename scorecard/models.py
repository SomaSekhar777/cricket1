from django.db import models

class Teams(models.Model):
    country_name = models.CharField(max_length=100)
    total_runs = models.IntegerField()
    total_wickets = models.IntegerField()
    total_overs = models.CharField(max_length=10)
    
