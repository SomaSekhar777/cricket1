# Generated by Django 4.2.2 on 2023-07-15 08:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0012_alter_somasekhar_profile_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='somasekhar',
            name='profile_picture',
            field=models.ImageField(blank=True, default='static/dummy.png', null=True, upload_to='images/'),
        ),
    ]
