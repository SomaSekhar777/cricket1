# Generated by Django 4.2.2 on 2023-07-11 11:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('school', '0005_emailfield'),
    ]

    operations = [
        migrations.CreateModel(
            name='course',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course_name', models.CharField(max_length=10)),
                ('activation', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='learner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_name', models.CharField(max_length=20)),
                ('courses', models.ManyToManyField(to='school.course')),
            ],
        ),
    ]
