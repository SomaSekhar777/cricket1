# Generated by Django 4.2.2 on 2023-06-30 08:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chaibandi', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='total_earnings',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_earnings', models.IntegerField()),
                ('stock', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chaibandi.stock')),
            ],
        ),
    ]
