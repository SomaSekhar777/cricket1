from django.db import models


class stock(models.Model):
    milk = models.IntegerField()
    tea = models.IntegerField()
    total_earning = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.milk} - {self.tea}'
    
class total_earnings(models.Model):
    total_earnings = models.IntegerField()
    stock = models.ForeignKey(stock, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.total_earnings}'

# Create your models here.
