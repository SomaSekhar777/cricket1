from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path("sayhello/", views.say_hello, name = 'say_hello'),
    path('show_stall/', views.show_stall_ , name='show_stall'),
    path('add_stock/', views.add_stock_, name='add_stock'),
    path('show_earnings', views.show_earnings_, name='show_earnings')
]

