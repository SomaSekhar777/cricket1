from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.files import File
import random
from .models import stock


def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')

list1 = [0]
def show_stall(request):
    stock_ = stock.objects.get(id=1)
    if request.method == 'POST':
        if request.POST.get('regular_tea') == 'Regular Tea - 15.00 /-':
            list1[0] = list1[0] + 15
            stock_.milk  = stock_.milk - 150
            stock_.tea = stock_.tea - 10
        if request.POST.get('black_tea') == 'Black Tea - 10.00 /-':
            list1[0] = list1[0] + 10
            stock_.tea = stock_.tea - 10
        stock_.save()
        return redirect('show_stall')
    
    else:
        milk_litres = stock_.milk // 1000
        milk_ml = stock_.milk % 1000
        tea_litres = stock_.tea // 1000
        tea_ml = stock_.tea % 1000
        context = {
            'milk_l' : milk_litres,
            'milk_ml' : milk_ml,
            'tea_l' : tea_litres,
            'tea_ml' : tea_ml
        }
        return render(request, 'chaibandi/index.html', context)


def show_earnings(request):
    total = list1[0]
    return HttpResponse(f'Total Earnings = {total}')


def add_stock(request):
    stock_ = stock.objects.get(id=1)
    if request.method == 'POST':
        stock_.milk = stock_.milk + int(request.POST['milk']) * 1000
        stock_.tea = stock_.tea + int(request.POST['tea']) * 1000
        stock_.save()
        list1[0] = list1[0] - int(request.POST['milk']) * 40
        list1[0] = list1[0] - int(request.POST['tea']) * 200
        return redirect('show_stall')
    else:
        return render(request, 'chaibandi/add_stock.html')
    

def show_stall_(request):
    stock_ = stock.objects.get(id=1)

    if request.method == 'POST':
        if request.POST.get('regular_tea') == 'Regular Tea - 15.00 /-':
            stock_.total_earning += 15
            stock_.milk  = stock_.milk - 150
            stock_.tea = stock_.tea - 10
        if request.POST.get('black_tea') == 'Black Tea - 10.00 /-':
            stock_.total_earning += 10
            stock_.tea = stock_.tea - 10
        stock_.save()
        return redirect('show_stall')
    
    else:
        milk_litres = stock_.milk // 1000
        milk_ml = stock_.milk % 1000
        tea_litres = stock_.tea // 1000
        tea_ml = stock_.tea % 1000
        context = {
            'milk_l' : milk_litres,
            'milk_ml' : milk_ml,
            'tea_l' : tea_litres,
            'tea_ml' : tea_ml
        }
        return render(request, 'chaibandi/index.html', context)
    
def show_earnings_(request):
    stock_ = stock.objects.get(id=1)
    total = stock_.total_earning
    return HttpResponse(f'Total Earnings = {total}')

def add_stock_(request):
    stock_ = stock.objects.get(id=1)
    if request.method == 'POST':
        stock_.milk = stock_.milk + int(request.POST['milk']) * 1000
        stock_.tea = stock_.tea + int(request.POST['tea']) * 1000
        stock_.save()
        stock_.total_earning -= int(request.POST['milk']) * 40
        stock_.total_earning -= int(request.POST['tea']) * 200
        return redirect('show_stall')
    else:
        return render(request, 'chaibandi/add_stock.html')

    


'''def add_stock(request):
    stock_ = stock.objects.get(id=1)
    if request.method == 'POST':
        if request.POST['milk'] == None or request.POST['milk'] == '':
            stock_.milk = stock_.milk + 0
            stock_.tea = stock_.tea + int(request.POST['tea']) * 1000
        if request.POST['tea'] == None or request.POST['tea'] == '':
            stock_.milk = stock_.milk + 0
        stock_.milk = stock_.milk + int(request.POST['milk']) * 1000
        stock_.tea = stock_.tea + int(request.POST['tea']) * 1000
        stock_.save()
        list1[0] = list1[0] - int(request.POST['milk']) * 40
        list1[0] = list1[0] - int(request.POST['tea']) * 200
        return redirect('show_stall')
    else:
        return render(request, 'chaibandi/add_stock.html')
'''

    


