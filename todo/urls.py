from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('todolist/', views.Show_todo, name ='todolist'),
    path('edit_details/<int:id>', views.edit_todo, name="edit_todo"),
    path('delete_todo/<int:id>' , views.delete_todo, name = "delete_todo"),
    path('add/', views.add_todo, name = "add_todo"),
    path('todolist1/', views.Show_todo_, name ='todolist_')
]