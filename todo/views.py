from django.shortcuts import render, redirect
from .models import Todo

# Create your views here.

def Show_todo(request):
    todo_list = Todo.objects.all().order_by('due_date')
    high_list = []
    medium_list = []
    low_list = []
    for todo in todo_list:
        if todo.priority == "High":
            high_list.append(todo)
        elif todo.priority == "Medium":
            medium_list.append(todo)
        else:
            low_list.append(todo)
    for object in medium_list:
        high_list.append(object)
    for object in low_list:
        high_list.append(object)
    return render(request, 'todo/index.html', {'todo_list' : high_list})

def add_todo(request):
    if request.method == "GET":
        return render(request, 'todo/add_todo.html')
        
    else:
        todo = Todo(
            task = request.POST['task'],
            priority = request.POST['priority'],
            due_date =  request.POST['date']
        )
        todo.save()
        return redirect('todolist')


def edit_todo(request,id):
    todo = Todo.objects.get(id = id)
    if request.method == 'GET':
        context = {'todo': todo}
        return render(request, 'todo/edit_detail.html', context)
    else:
        todo.task = request.POST['task']
        todo.priority = request.POST['priority']
        todo.due_date = request.POST['date']
        todo.save()
        return redirect('todolist')
    
def delete_todo(request, id):
    todo = Todo.objects.get(id = id)
    todo.delete()
    return redirect('todolist')

def Show_todo_(request):
    todo_list = Todo.objects.all().order_by('due_date')
    high_list = []
    medium_list = []
    low_list = []
    for todo in todo_list:
        if todo.priority == "High":
            high_list.append(todo)
        elif todo.priority == "Medium":
            medium_list.append(todo)
        else:
            low_list.append(todo)
    for object in medium_list:
        high_list.append(object)
    for object in low_list:
        high_list.append(object)
    return render(request, 'todo/index2.html', {'todo_list' : high_list})
